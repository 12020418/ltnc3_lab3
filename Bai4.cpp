#include <iostream>
#include <string.h>
using namespace std;
struct Sinhvien{
    string hoten;
    int diem;
    Sinhvien *next;
};
Sinhvien *head=NULL;
Sinhvien *tail=NULL;

Sinhvien *tao(string hoten, int diem){
    Sinhvien *p = new Sinhvien;
    p->hoten= hoten;
    p->diem = diem;
    p->next = NULL;
    return p;
}

void Nhap(){
    string hoten;
    int diem;
    Sinhvien *p;
    
    cout<<"Nhap ten: ";
    getline(cin,hoten);
    cout<<"Nhap diem: ";
    cin>>diem;
    p = tao(hoten, diem);
    
    if (head==NULL && tail==NULL) {
        head=p;
        tail=p;
    }else{
        tail->next = p;
        tail = p;
    }
    cin.ignore(1);
}

void Hienthi(){
    Sinhvien *p;
    p = head;
    while (p!=NULL) {
        cout<<p->hoten<<"-"<<p->diem<<endl;
        p=p->next;
    }
    cout<<"-----------------------------------\n";
}

void Swap(Sinhvien *a,Sinhvien *b){
    string ten;
    int diem;
    ten = a->hoten;
    a->hoten=b->hoten;
    b->hoten = ten;
    diem=a->diem;
    a->diem=b->diem;
    b->diem=diem;
}
string TrichTen(string a){
    for(int i=a.length()-1;i>=0;i--){
        if(a[i]==' '){
            return a.substr(i+1,a.length()-i);
        }
    }
    return a;
}
void Sxten(){
    Sinhvien *p;
    bool check=true;
    while (check) {
        check=false;
        p=head;
        while (p->next!=NULL) {
            string ten1=TrichTen(p->hoten);
            string ten2=TrichTen(p->next->hoten);
            if(ten1.compare(ten2)>0){
                Swap(p, p->next);
                check=true;
            }
            p=p->next;
        }
    }
}
void Sxdiem(){
    Sinhvien *p;
    bool check=true;
    while (check) {
        check=false;
        p=head;
        while (p->next!=NULL) {
            if(p->diem < p->next->diem){
                Swap(p, p->next);
                check=true;
            }
            p=p->next;
        }
    }
}
void TimSv(){
    Sinhvien *p;
    int a,b;
    cout<<"Nhap khoang diem can tim: \n";
    cin>>a>>b;
    p=head;
    bool check=true;
    while (p!=NULL) {
        if (a<=p->diem&&p->diem<=b) {
            check=false;
            cout<<p->hoten<<"-"<<p->diem<<endl;
        }
        p=p->next;
    }
    if (check) {
        cout<<"Khong co gia tri nao thoa man khoang can tim\n";
    }
    cout<<"--------------------------------\n";
}

void TimTen(){
    string ten;
    bool check=true;
    Sinhvien *p;
    p=head;
    cout<<"Nhap ten can tim: ";
    getline(cin,ten);
    while (p!=NULL) {
        if (ten.compare(TrichTen(p->hoten))==0) {
            cout<<p->hoten<<"-"<<p->diem<<endl;
            check=false;
        }
        p=p->next;
    }
    if (check) {
        cout<<"Khong co ten nao giong ten ban nhap\n";
    }
    cin.ignore(1);
}
int main(){
    int check=1;
    cout<<"1. Nhap danh sach\n";
    cout<<"2. Hien thi danh sach\n";
    cout<<"3. Sap xep ten\n";
    cout<<"4. Sap xep theo diem thap dan\n";
    cout<<"5. Tim kiem sinh vien co diem tu a den b\n";
    cout<<"6. Tim kiem ten\n";
    string a="asd";
    while (check) {
        switch(check){
            case 1:
                Nhap();
                break;
            case 2:
                Hienthi();
                break;
            case 3:
                Sxten();
                break;
            case 4:
                Sxdiem();
                break;
            case 5:
                TimSv();
                break;
            case 6:
                TimTen();
                break;
        }
        cout<<"dung lai? (go 0)";
        cin>>check;
        cin.ignore(1);
    }
    return 0;
}

